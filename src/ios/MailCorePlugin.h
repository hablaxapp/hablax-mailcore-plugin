#import <Cordova/CDV.h>
#import <MailCore/MailCore.h>

@interface MailCorePlugin : CDVPlugin

@property(nonatomic, strong) MCOIMAPSession *imapSession;
@property(nonatomic, strong) MCOSMTPSession *smtpSession;

@property(nonatomic, strong) MCOIMAPIdleOperation *idleOperation;

@property(nonatomic, strong) NSMutableArray *idleOperations;

@property(nonatomic, strong) NSString* hablaxServerEmail;
@property(nonatomic, strong) NSArray<NSString*>* hablaxEmailsAccepted;

@property(nonatomic, assign) int step;
@property(nonatomic, assign) uid_t _lastKnownMessageUID;
@property(nonatomic, assign) bool _listenRunning;





- (void) authenticate:(CDVInvokedUrlCommand*)command;
- (void) imapFetchAllMessages:(CDVInvokedUrlCommand*)command;
- (void) smtpSendMessage:(CDVInvokedUrlCommand*)command;
- (void) listenNewMessages:(CDVInvokedUrlCommand*)command;
- (void) killListenerOperations:(CDVInvokedUrlCommand*)command;
- (void) updateHablaxServerEmail:(CDVInvokedUrlCommand*)command;
- (void) updateHablaxEmailsAccepted:(CDVInvokedUrlCommand*)command;


@end
