#import "MailCorePlugin.h"


@implementation MailCorePlugin


- (void) authenticate:(CDVInvokedUrlCommand*)command {
    NSString* hostname = [[command arguments] objectAtIndex:0]; // imap.gmail.com
    int port = [[[command arguments] objectAtIndex:1] intValue]; // 993
    NSString* username = [[command arguments] objectAtIndex:2]; // enrique.bembassat@nauta.cu
    NSString* password = [[command arguments] objectAtIndex:3]; // net3kk*30
    NSString* connectionType = [[command arguments] objectAtIndex:4]; // TLS, StartTLS, or Clear
    
    
    [self.commandDelegate runInBackground:^{
        MCOIMAPSession *session = [[MCOIMAPSession alloc] init];
        [session setHostname:hostname];
        [session setPort:port];
        [session setUsername:username];
        [session setPassword:password];
        [session setTimeout:60]; // 60s
        if ([connectionType isEqualToString:@"TLS"]) {
            [session setConnectionType:MCOConnectionTypeTLS];
        } else if ([connectionType isEqual:@"StartTLS"]) {
            [session setConnectionType:MCOConnectionTypeStartTLS];
        } else if ([connectionType isEqual:@"Clear"]) {
            [session setConnectionType:MCOConnectionTypeClear];
        }
        
        
        NSString *folder = @"INBOX";
        MCOIMAPMessagesRequestKind requestKind = MCOIMAPMessagesRequestKindHeaders;
        MCOIndexSet *uids = [MCOIndexSet indexSetWithRange:MCORangeMake(1, 1)];
        
        MCOIMAPFetchMessagesOperation *fetchOperation = [session fetchMessagesOperationWithFolder:folder requestKind:requestKind uids:uids];
        
        [fetchOperation start:^(NSError * error, NSArray * fetchedMessages, MCOIndexSet * vanishedMessages) {

    
            if(error) {
                CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.localizedDescription];
                [pluginResult setKeepCallback:[NSNumber numberWithBool:NO]];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                return;
            }
            
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [pluginResult setKeepCallback:[NSNumber numberWithBool:NO]];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
        
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_NO_RESULT];
        [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void) syncMessages:(CDVInvokedUrlCommand*)command {
    NSString* hostname = [[command arguments] objectAtIndex:0]; // smtp.gmail.com
    int port = [[[command arguments] objectAtIndex:1] intValue]; // 465
    NSString* username = [[command arguments] objectAtIndex:2]; // vinh.thien0301@gmail.com
    NSString* password = [[command arguments] objectAtIndex:3]; // xxxx
    NSString* connectionType = [[command arguments] objectAtIndex:4]; // TLS, StartTLS, or Clear
    uint64_t lastKnownMessageUID = [[[command arguments] objectAtIndex:5] unsignedLongLongValue];
    self.hablaxServerEmail = [[command arguments] objectAtIndex:6];
    self.hablaxEmailsAccepted = [[command arguments] objectAtIndex:7];
    
    [self.commandDelegate runInBackground:^{
        
        MCOIMAPSession *session = [[MCOIMAPSession alloc] init];
        [session setHostname:hostname];
        [session setPort:port];
        [session setUsername:username];
        [session setPassword:password];
        [session setTimeout:60]; // 60s
        if ([connectionType isEqualToString:@"TLS"]) {
            [session setConnectionType:MCOConnectionTypeTLS];
        } else if ([connectionType isEqual:@"StartTLS"]) {
            [session setConnectionType:MCOConnectionTypeStartTLS];
        } else if ([connectionType isEqual:@"Clear"]) {
            [session setConnectionType:MCOConnectionTypeClear];
        }
        
//        self._listenRunning = false;
        
        NSString *folder = @"INBOX";
        
        NSArray<NSString*>* acceptedEmails = [self parseEmailsString:self.hablaxEmailsAccepted];
        
        MCOIMAPSearchExpression *fromEmailsExpression;
        if (acceptedEmails && acceptedEmails.count > 0) {
            fromEmailsExpression = [MCOIMAPSearchExpression searchFrom:[acceptedEmails objectAtIndex:0]];
            for (int index = 1; index < acceptedEmails.count; index++) {
                fromEmailsExpression = [MCOIMAPSearchExpression searchOr:[MCOIMAPSearchExpression searchFrom:[acceptedEmails objectAtIndex:index]] other:fromEmailsExpression];
            }
        }
        
        if (fromEmailsExpression == nil) {
            fromEmailsExpression = [MCOIMAPSearchExpression searchFrom:self.hablaxServerEmail];
        } else {
            fromEmailsExpression = [MCOIMAPSearchExpression searchOr:[MCOIMAPSearchExpression searchFrom:self.hablaxServerEmail] other:fromEmailsExpression];
        }
        
        
        MCOIMAPSearchExpression * expression = [MCOIMAPSearchExpression searchAnd:[MCOIMAPSearchExpression searchUnread] other:fromEmailsExpression];
        
        MCOIMAPSearchOperation* searchOperation = [session searchExpressionOperationWithFolder:folder expression:expression];
        
        [searchOperation start:^(NSError * _Nullable error, MCOIndexSet * _Nullable searchResult) {
            if(error) {
                CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.localizedDescription];
                [pluginResult setKeepCallback:[NSNumber numberWithBool:NO]];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                return;
            }
            
            if (!searchResult) {
                NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                                   initWithObjectsAndKeys :
                                                   [NSNumber numberWithUnsignedLongLong:lastKnownMessageUID], @"last_known_message_uid",
                                                   [NSNumber numberWithInteger:self.step++], @"step",@"syn-search-noresult",@"des",
                                                   nil
                                                   ];
                
                CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
                [pluginResult setKeepCallback:[NSNumber numberWithBool:NO]];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                return;
            }
            
            MCORange* ranges = searchResult.allRanges;
            if (!ranges || searchResult.rangesCount == 0) {
                NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                                   initWithObjectsAndKeys :
                                                   [NSNumber numberWithUnsignedLongLong:lastKnownMessageUID], @"last_known_message_uid",
                                                   [NSNumber numberWithInteger:self.step++], @"step",@"syn-search-zerocount",@"des",
                                                   nil
                                                   ];
                
                CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
                [pluginResult setKeepCallback:[NSNumber numberWithBool:NO]];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                return;
            }
            
            if (ranges && searchResult.rangesCount > 0 ) {
                MCORange lastRange = ranges[searchResult.rangesCount - 1];
                self._lastKnownMessageUID = MCORangeRightBound(lastRange);
            }
            
                
            NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                               initWithObjectsAndKeys :
                                               [NSNumber numberWithUnsignedLongLong:self._lastKnownMessageUID], @"last_known_message_uid",
                                               [NSNumber numberWithInteger:self.step++], @"step",@"syn-search-hascount",@"des",
                                               nil
                                               ];
            
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
            [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            
            
            
            MCOIMAPMessagesRequestKind requestKind = MCOIMAPMessagesRequestKindHeaders | MCOIMAPMessagesRequestKindStructure;
            
            MCOIMAPFetchMessagesOperation *fetchOperation = [session fetchMessagesOperationWithFolder:folder requestKind:requestKind uids:searchResult];
            
            [fetchOperation start:^(NSError * error, NSArray * fetchedMessages, MCOIndexSet * vanishedMessages) {
                
                if(error) {
                    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.localizedDescription];
                    [pluginResult setKeepCallback:[NSNumber numberWithBool:NO]];
                    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                    return;
                }
                
                NSMutableArray *mails = [[NSMutableArray alloc] init];
                
                if (fetchedMessages && fetchedMessages.count > 0) {
                    MCOIMAPMessage *message = fetchedMessages.lastObject;
                    self._lastKnownMessageUID = message.uid;
                    
                    MCOIndexSet *uids = [MCOIndexSet indexSet];
                    
                    for (MCOIMAPMessage* fetchedMessage in fetchedMessages) {
                        NSMutableDictionary* mail = [self javascriptMessage:fetchedMessage];
                        [mails addObject:mail];
                        
                        if (![[mail objectForKey:@"hasAttachment"] boolValue]) {
                            [uids addIndex:fetchedMessage.uid];
                        }
                    }
                    
                    MCOIMAPOperation* storeFlagsOperation = [session storeFlagsOperationWithFolder:folder uids:uids kind:MCOIMAPStoreFlagsRequestKindSet flags:MCOMessageFlagDeleted|MCOMessageFlagSeen];
                    [storeFlagsOperation start:^(NSError * _Nullable error) {
                        // nothing
                    }];
                }
                
                NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                                   initWithObjectsAndKeys :
                                                   mails, @"new_messages",
                                                   [NSNumber numberWithInteger:self.step++], @"step",@"syn-fetch-hascount",@"des",
                                                   nil
                                                   ];
                
                CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
                [pluginResult setKeepCallback:[NSNumber numberWithBool:NO]];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            }];
        }];
        
        
        
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_NO_RESULT];
        [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void) fetchAttachment:(CDVInvokedUrlCommand*)command {
    NSString* hostname = [[command arguments] objectAtIndex:0]; // smtp.gmail.com
    int port = [[[command arguments] objectAtIndex:1] intValue]; // 465
    NSString* username = [[command arguments] objectAtIndex:2]; // vinh.thien0301@gmail.com
    NSString* password = [[command arguments] objectAtIndex:3]; // xxxx
    NSString* connectionType = [[command arguments] objectAtIndex:4]; // TLS, StartTLS, or Clear
    uint64_t messageUID = [[[command arguments] objectAtIndex:5] unsignedLongLongValue];
    NSString* partID = [[command arguments] objectAtIndex:6];
    NSInteger partEncoding = [[[command arguments] objectAtIndex:7] integerValue];
    NSString* filename = [[command arguments] objectAtIndex:8];

    [self.commandDelegate runInBackground:^{
        
        MCOIMAPSession *session = [[MCOIMAPSession alloc] init];
        [session setHostname:hostname];
        [session setPort:port];
        [session setUsername:username];
        [session setPassword:password];
        [session setTimeout:60]; // 60s
        if ([connectionType isEqualToString:@"TLS"]) {
            [session setConnectionType:MCOConnectionTypeTLS];
        } else if ([connectionType isEqual:@"StartTLS"]) {
            [session setConnectionType:MCOConnectionTypeStartTLS];
        } else if ([connectionType isEqual:@"Clear"]) {
            [session setConnectionType:MCOConnectionTypeClear];
        }
        
        NSString *folder = @"INBOX";

        MCOIMAPFetchContentOperation *mcop = [session fetchMessageAttachmentOperationWithFolder:folder uid:messageUID partID:partID encoding:partEncoding];
        [mcop start:^(NSError *error, NSData *data) {
            if(error) {
                CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.localizedDescription];
                [pluginResult setKeepCallback:[NSNumber numberWithBool:NO]];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                return;
            }
            
            MCOIndexSet *uids = [MCOIndexSet indexSet];
            [uids addIndex:messageUID];
            
            MCOIMAPOperation* storeFlagsOperation = [session storeFlagsOperationWithFolder:folder uids:uids kind:MCOIMAPStoreFlagsRequestKindSet flags:MCOMessageFlagDeleted|MCOMessageFlagSeen];
            [storeFlagsOperation start:^(NSError * _Nullable error) {
                // nothing
            }];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *saveDirectory = [paths objectAtIndex:0];
            NSString *attachmentPath = [saveDirectory stringByAppendingPathComponent:filename];
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:attachmentPath];
            if (fileExists) {
                NSLog(@"File already exists!");
            }
            else{
                [data writeToFile:attachmentPath atomically:YES];
            }
            
            
            NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                               initWithObjectsAndKeys :
                                               attachmentPath, @"attachmentPath",
                                               nil
                                               ];
            
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
            [pluginResult setKeepCallback:[NSNumber numberWithBool:NO]];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            
        }];
        
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_NO_RESULT];
        [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)imapFetchAllMessages:(CDVInvokedUrlCommand*)command {
    // nothing
}

- (NSMutableDictionary*) javascriptMessage:(MCOIMAPMessage*)imapMessage {
    
    NSNumber* uid = [NSNumber numberWithInt:imapMessage.uid];
    NSString* from = imapMessage.header.from.mailbox != nil? [[NSString alloc] initWithString:imapMessage.header.from.mailbox] : @"";
    NSString* to = @"";
    if (imapMessage.header.to && imapMessage.header.to.count > 0) {
        MCOAddress* toAddress = [imapMessage.header.to objectAtIndex:0];
        to = toAddress.mailbox != nil? [[NSString alloc] initWithString:toAddress.mailbox] : @"";
    }
    
    NSString* subject = imapMessage.header.subject != nil? [[NSString alloc] initWithString:imapMessage.header.subject] : @"";
    NSString* body = imapMessage.header.subject != nil? [[NSString alloc] initWithString:imapMessage.header.subject] : @"";
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
//    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
    NSString *dateString = [formatter stringFromDate:imapMessage.header.receivedDate];
    
    bool hasAttachment = false;
    NSString *partID = @"";
    NSInteger partEncoding = 0;
    NSString *filename = @"";
    
    
    
    if (imapMessage.attachments && [imapMessage.attachments count] > 0)
    {
        hasAttachment = true;
        MCOIMAPPart *part = [imapMessage.attachments objectAtIndex:0];
        partID = part.partID;
        partEncoding = part.encoding;
        filename = part.filename;
    }
    
    NSMutableDictionary* mail = [ [NSMutableDictionary alloc]
            initWithObjectsAndKeys :
            uid, @"uid",
            from, @"from",
            to, @"to",
            subject, @"subject",
            body, @"body",
            dateString, @"date",
             [NSNumber numberWithBool:hasAttachment], @"hasAttachment",
            partID, @"partID",
            [NSNumber numberWithInteger:partEncoding], @"partEncoding",
            filename, @"filename",
            nil
            ];
    
    return mail;
}

- (void) smtpSendMessage:(CDVInvokedUrlCommand*)command {
    NSString* hostname = [[command arguments] objectAtIndex:0]; // smtp.gmail.com
    int port = [[[command arguments] objectAtIndex:1] intValue]; // 465
    NSString* username = [[command arguments] objectAtIndex:2]; // vinh.thien0301@gmail.com
    NSString* password = [[command arguments] objectAtIndex:3]; // xxxx
    NSString* connectionType = [[command arguments] objectAtIndex:4]; // TLS, StartTLS, or Clear
    NSString* recipient = [[command arguments] objectAtIndex:5]; // fidel@gmail.com
    NSString* subject = [[command arguments] objectAtIndex:6];
    NSString* body = [[command arguments] objectAtIndex:7];
    NSString* attachmentPath = [[command arguments] objectAtIndex:8];

    [self.commandDelegate runInBackground:^{
        
        if (self.smtpSession == nil) {
            self.smtpSession = [[MCOSMTPSession alloc] init];
            [self.smtpSession setHostname:hostname];
            [self.smtpSession setPort:port];
            [self.smtpSession setUsername:username];
            [self.smtpSession setPassword:password];
            if ([connectionType isEqualToString:@"TLS"]) {
                [self.smtpSession setConnectionType:MCOConnectionTypeTLS];
            } else if ([connectionType isEqual:@"StartTLS"]) {
                [self.smtpSession setConnectionType:MCOConnectionTypeStartTLS];
            } else if ([connectionType isEqual:@"Clear"]) {
                [self.smtpSession setConnectionType:MCOConnectionTypeClear];
            }
        }
        
        MCOMessageBuilder * builder = [[MCOMessageBuilder alloc] init];
        [[builder header] setFrom:[MCOAddress addressWithDisplayName:nil mailbox:username]];
        NSMutableArray *to = [[NSMutableArray alloc] init];
        //for(NSString *toAddress in RECIPIENTS) {
        MCOAddress *newAddress = [MCOAddress addressWithMailbox:recipient];
        [to addObject:newAddress];
        //}
        [[builder header] setTo:to];
        NSMutableArray *cc = [[NSMutableArray alloc] init];
        //for(NSString *ccAddress in CC) {
        //    MCOAddress *newAddress = [MCOAddress addressWithMailbox:ccAddress];
        //    [cc addObject:newAddress];
        //}
        [[builder header] setCc:cc];
        NSMutableArray *bcc = [[NSMutableArray alloc] init];
        //for(NSString *bccAddress in BCC) {
        //    MCOAddress *newAddress = [MCOAddress addressWithMailbox:bccAddress];
        //    [bcc addObject:newAddress];
        //}
        [[builder header] setBcc:bcc];
        [[builder header] setSubject:subject];
        
        if (![body isKindOfClass:[NSNull class]]) {
            [builder setTextBody:body];
        }
        
        if(![attachmentPath isKindOfClass:[NSNull class]]) {
            
//            NSString imageAttachment = [NSString stringWithFormat:@"<img src='%@' />", attachmentPath];
//            MCOAttachment *attachment = [MCOAttachment attachmentWithContentsOfFile:imageAttachment];
//            [builder addAttachment:attachment];
           NSString *correctedAttachmentPath = [attachmentPath substringWithRange:NSMakeRange(7, attachmentPath.length - 7)];

            MCOAttachment *attachment = [MCOAttachment attachmentWithContentsOfFile:correctedAttachmentPath];
            [builder addAttachment:attachment];
        }
     
        
        NSData * rfc822Data = [builder data];

        MCOSMTPSendOperation *sendOperation = [self.smtpSession sendOperationWithData:rfc822Data];
        [sendOperation start:^(NSError *error) {
            if(error) {
                NSString* ss = error.localizedDescription;
                NSLog(@"%@ Error sending email:%@", username, error);
                CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.localizedDescription];
                [pluginResult setKeepCallback:[NSNumber numberWithBool:NO]];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                return;
            }
            
            NSLog(@"%@ Successfully sent email!", username);
            NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                               initWithObjectsAndKeys :
                                               @"Sent successful", @"message",
                                               nil
                                               ];
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
            [pluginResult setKeepCallback:[NSNumber numberWithBool:NO]];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
        
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_NO_RESULT];
        [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void) listenNewMessages:(CDVInvokedUrlCommand*)command {
    
    NSString* hostname = [[command arguments] objectAtIndex:0]; // smtp.gmail.com
    int port = [[[command arguments] objectAtIndex:1] intValue]; // 465
    NSString* username = [[command arguments] objectAtIndex:2]; // vinh.thien0301@gmail.com
    NSString* password = [[command arguments] objectAtIndex:3]; // xxxx
    NSString* connectionType = [[command arguments] objectAtIndex:4]; // TLS, StartTLS, or Clear
    uint64_t lastKnownMessageUID = [[[command arguments] objectAtIndex:5] unsignedLongLongValue];
    self.hablaxServerEmail = [[command arguments] objectAtIndex:6];
    self.hablaxEmailsAccepted = [[command arguments] objectAtIndex:7];
    
    [self.commandDelegate runInBackground:^{
        
        self.imapSession = [[MCOIMAPSession alloc] init];
        [self.imapSession setHostname:hostname];
        [self.imapSession setPort:port];
        [self.imapSession setUsername:username];
        [self.imapSession setPassword:password];
        [self.imapSession setTimeout:60]; // 60s
        if ([connectionType isEqualToString:@"TLS"]) {
            [self.imapSession setConnectionType:MCOConnectionTypeTLS];
        } else if ([connectionType isEqual:@"StartTLS"]) {
            [self.imapSession setConnectionType:MCOConnectionTypeStartTLS];
        } else if ([connectionType isEqual:@"Clear"]) {
            [self.imapSession setConnectionType:MCOConnectionTypeClear];
        }
        
        
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_NO_RESULT];
        [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
        self._listenRunning = false;
        
        // stop all idle operations before start listenning
        if (self.idleOperation && !self.idleOperation.isCancelled) {
            [self.idleOperation cancel];
            
            // wait a time for stop previous idle operation completely
            [NSThread sleepForTimeInterval:6.00f]; // 60 miliseconds
        }
        
        
        self._listenRunning = true;
        
        
        while (self._listenRunning) {
            
            if (self.idleOperation && !self.idleOperation.isCancelled) {
                [NSThread sleepForTimeInterval:5.0f]; // 50 miliseconds
                continue;
            }
            
//            if (self._lastKnownMessageUID < lastKnownMessageUID) {
//                self._lastKnownMessageUID = lastKnownMessageUID;
//            }
            
            NSString *folder = @"INBOX";
            self.idleOperation = [self.imapSession idleOperationWithFolder:folder lastKnownUID:self._lastKnownMessageUID];
        
            if (!self.idleOperations) {
                self.idleOperations = [[NSMutableArray alloc] init];
            }
            [self.idleOperations addObject:self.idleOperation];
            
            NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                               initWithObjectsAndKeys :
                                               [NSNumber numberWithUnsignedLongLong:self._lastKnownMessageUID], @"last_known_message_uid",
                                               [NSNumber numberWithInteger:self.step++], @"step",@"listen-start",@"des",
                                               nil
                                               ];
            
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
            [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            
            void(^idleHandler)(NSError *error) = ^(NSError *error) {
                if(error) {
                    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.localizedDescription];
                    [pluginResult setKeepCallback:[NSNumber numberWithBool:NO]];
                    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                    return;
                }
               
                if (self._lastKnownMessageUID < lastKnownMessageUID) {
                    self._lastKnownMessageUID = lastKnownMessageUID;
                }
                
                UInt64 start = self._lastKnownMessageUID + 1;
                UInt64 end = UINT64_MAX;
                
                MCOIMAPMessagesRequestKind requestKind = MCOIMAPMessagesRequestKindHeaders | MCOIMAPMessagesRequestKindStructure;
                MCOIndexSet *uids = [MCOIndexSet indexSetWithRange:MCORangeMake(start, end)];
                
                MCOIMAPFetchMessagesOperation *fetchOperation = [self.imapSession fetchMessagesOperationWithFolder:folder requestKind:requestKind uids:uids];
                
                [fetchOperation start:^(NSError * _Nullable error, NSArray * _Nullable fetchedMessages, MCOIndexSet * _Nullable vanishedMessages) {
                
                
                        if(error) {
                            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.localizedDescription];
                            [pluginResult setKeepCallback:[NSNumber numberWithBool:NO]];
                            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                            return;
                        }
                        
                        if (fetchedMessages && fetchedMessages.count > 0) {
                            
                            
                            
                            
                            MCOIMAPMessage *message = fetchedMessages.lastObject;
                            self._lastKnownMessageUID = message.uid;
                            
                            NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                                               initWithObjectsAndKeys :
                                                               [NSNumber numberWithUnsignedLongLong:self._lastKnownMessageUID], @"last_known_message_uid",
                                                               [NSNumber numberWithInteger:self.step++], @"step",@"listen-delete-seenmessages",@"des",
                                                               nil
                                                               ];
                            
                            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
                            [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
                            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                            
                        }
                        
                        
                        NSMutableArray *mails = [[NSMutableArray alloc] init];
                        
                        if (fetchedMessages && fetchedMessages.count > 0) {
                            
                            MCOIndexSet *uids = [MCOIndexSet indexSet];
                            
                            for (MCOIMAPMessage* fetchedMessage in fetchedMessages) {
                                
                                if ([[[[fetchedMessage header] from] mailbox] isEqualToString:self.hablaxServerEmail]) {
                                    
                                    NSMutableDictionary* mail = [self javascriptMessage:fetchedMessage];
                                    [mails addObject:mail];
                                    if (![[mail objectForKey:@"hasAttachment"] boolValue]) {
                                        [uids addIndex:fetchedMessage.uid];
                                    }
                                    
                                }
                                
                                
                            }
                            
                            MCOIMAPOperation* storeFlagsOperation = [self.imapSession storeFlagsOperationWithFolder:folder uids:uids kind:MCOIMAPStoreFlagsRequestKindSet flags:MCOMessageFlagDeleted|MCOMessageFlagSeen];
                            [storeFlagsOperation start:^(NSError * _Nullable error) {
                                // nothing
                            }];
                        }
                        
                        NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                                           initWithObjectsAndKeys :
                                                           mails, @"new_messages",
                                                           [NSNumber numberWithInteger:self.step++], @"step",@"listen-fetch-hascount",@"des",
                                                           nil
                                                           ];
                        
                        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
                        [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
                        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                        
                        [self.idleOperation cancel];

                }]; // end of fetch messages operation
    
                
                
            }; // end of idle handler
            
            
            [self.idleOperation start:idleHandler];
        } // end of while
        
        if (self.idleOperation && !self.idleOperation.isCancelled) {
            [self.idleOperation cancel];
        }
        
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_NO_RESULT];
        [pluginResult setKeepCallback:[NSNumber numberWithBool:NO]];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
    }];
}

- (void) killListenerOperations:(CDVInvokedUrlCommand*)command {

    [self.commandDelegate runInBackground:^{
        
        self._listenRunning = false;
        
        for (MCOIMAPIdleOperation* idleOp in self.idleOperations) {
            if (!idleOp.isCancelled) {
                [idleOp interruptIdle];
            }
        }
        
        [self.idleOperations removeAllObjects];
    
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
    }];
}

- (void) updateHablaxServerEmail:(CDVInvokedUrlCommand*)command {
    NSString* hostname = [[command arguments] objectAtIndex:0]; // smtp.gmail.com
    int port = [[[command arguments] objectAtIndex:1] intValue]; // 465
    NSString* username = [[command arguments] objectAtIndex:2]; // vinh.thien0301@gmail.com
    NSString* password = [[command arguments] objectAtIndex:3]; // xxxx
    NSString* connectionType = [[command arguments] objectAtIndex:4]; // TLS, StartTLS, or Clear
    self.hablaxServerEmail = [[command arguments] objectAtIndex:5];
    
    
    [self.commandDelegate runInBackground:^{
        
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
    }];
}
- (void) updateHablaxEmailsAccepted:(CDVInvokedUrlCommand*)command {
    NSString* hostname = [[command arguments] objectAtIndex:0]; // smtp.gmail.com
    int port = [[[command arguments] objectAtIndex:1] intValue]; // 465
    NSString* username = [[command arguments] objectAtIndex:2]; // vinh.thien0301@gmail.com
    NSString* password = [[command arguments] objectAtIndex:3]; // xxxx
    NSString* connectionType = [[command arguments] objectAtIndex:4]; // TLS, StartTLS, or Clear
    self.hablaxEmailsAccepted = [[command arguments] objectAtIndex:5];
    
    [self.commandDelegate runInBackground:^{
        
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
    }];
}

- (NSArray<NSString *>*) parseEmailsString: (NSArray<NSString *>*) emails {
    if ([emails isKindOfClass:[NSNull class]]) {
        return nil;
    }
    
//    NSArray<NSString *> *emails = [emailString componentsSeparatedByString:@"/"];
    
    return emails;
}

@end

