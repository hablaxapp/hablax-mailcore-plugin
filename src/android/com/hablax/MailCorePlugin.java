package com.hablax;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;

import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPStore;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.AccessController;
import java.security.Provider;
import java.security.Security;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.CommandMap;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.MailcapCommandMap;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.event.MessageCountEvent;
import javax.mail.event.MessageCountListener;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.AndTerm;
import javax.mail.search.FlagTerm;
import javax.mail.search.FromTerm;
import javax.mail.search.SearchTerm;
import javax.mail.util.ByteArrayDataSource;

public class MailCorePlugin extends CordovaPlugin {

    static {
        Security.addProvider(new JSSEProvider());
    }

    private long _lastKnownMessageUID = 0L;
    private IdleThread idleThread;

    @Override
    public boolean execute(final String action, final JSONArray args, final CallbackContext command) throws JSONException {
        final Context context = this.cordova.getActivity();
        this.cordova.getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (action.equals("authenticate")) {
                    try {
                        final String hostname = args.get(0).toString();
                        final String port = args.get(1).toString();
                        final String username = args.get(2).toString();
                        final String password = args.get(3).toString();
                        final String connectionType = args.get(4).toString();

                        AsyncTask<String, Integer, Void> fetchMessagesTask = new AsyncTask<String, Integer, Void>() {
                            @Override
                            protected Void doInBackground(String... strings) {
                                try {
                                    String storeType = "imaps";

                                    Properties properties = new Properties();
                                    if (hostname.equals("imap.nauta.cu")) {
                                        properties.put("mail.imap.ssl.enable", false);
                                        properties.put("mail.imap.timeout", 60000);
                                        properties.put("mail.store.protocol", storeType);
                                    }
                                    properties.put("mail.imap.port", port);

                                    Session session = Session.getInstance(properties);
                                    Store store = session.getStore(storeType);
                                    store.connect(hostname, username, password);



                                    Folder emailFolder = store.getFolder("INBOX");
                                    if (emailFolder.isOpen()) {
                                        PluginResult dataResult = new PluginResult(PluginResult.Status.OK);
                                        dataResult.setKeepCallback(false);
                                        command.sendPluginResult(dataResult);
                                    } else {
                                        PluginResult dataResult = new PluginResult(PluginResult.Status.OK);
                                        dataResult.setKeepCallback(false);
                                        command.sendPluginResult(dataResult);
                                    }

                                    emailFolder.close(false);
                                    store.close();





                                } catch (NoSuchProviderException e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                } catch (MessagingException e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                } catch (Exception e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                }
                                return null;

                            }
                        };
                        fetchMessagesTask.execute();

                        PluginResult dataResult = new PluginResult(PluginResult.Status.NO_RESULT);
                        dataResult.setKeepCallback(true);
                        command.sendPluginResult(dataResult);
                    } catch (JSONException e) {
                        command.error(e.getMessage());
                    }

                }
                else if (action.equals("syncMessages")) {
                    try {
                        final String hostname = args.get(0).toString();
                        final String port = args.get(1).toString();
                        final String username = args.get(2).toString();
                        final String password = args.get(3).toString();
                        final String connectionType = args.get(4).toString();
                        final long lastMessageKnownUID = Long.parseLong(args.get(5).toString());
                        final String hablaxServerEmail = args.get(6).toString();
                        final JSONArray hablaxEmailsAccepted;
                        if (args.get(7) instanceof JSONArray) {
                            hablaxEmailsAccepted = (JSONArray)args.get(7);
                        } else if (args.get(7) instanceof JSONObject) {
                            hablaxEmailsAccepted = null;
                        }


                        AsyncTask<String, Integer, Boolean> syncMessagesTask = new AsyncTask<String, Integer, Boolean>() {
                            @Override
                            protected Boolean doInBackground(String[] objects) {
                                try {
                                    IMAPStore store = null;

                                    Properties properties = new Properties();
                                    if (hostname.equals("imap.nauta.cu")) {

                                        properties.put("mail.imap.ssl.enable", false);
                                        properties.put("mail.imap.timeout", "60000");
                                        Session session = Session.getDefaultInstance(properties, null);
                                        store = (IMAPStore)session.getStore("imap");

                                    } else if (hostname.equals("siamopizza.com")) {

//                                        properties.put("mail.store.protocol", "imaps");
//                                        properties.put("mail.imap.host", hostname);
//                                        properties.put("mail.imap.auth", "true");
//                                        properties.put("mail.imap.port", port);

                                        properties.put("mail.imap.ssl.enable", false);
                                        properties.put("mail.imap.timeout", "60000");
                                        properties.put("mail.debug", "true");

                                        Session session = Session.getDefaultInstance(properties, null);
                                        store = (IMAPStore)session.getStore("imap");
                                    } else {

                                        //                                        properties.setProperty("mail.store.protocol", "imaps");
                                        properties.setProperty("mail.imap.host", hostname);
                                        properties.setProperty("mail.imap.port", port);

                                        properties.put("mail.imap.ssl.enable", true);
                                        properties.setProperty("mail.imap.timeout", "60000");

                                        properties.put("mail.debug", "true");

                                        Session session = Session.getDefaultInstance(properties, null);
                                        store = (IMAPStore)session.getStore("imaps");
                                    }

                                    store.connect(hostname, username, password);
                                    IMAPFolder inbox = (IMAPFolder)store.getFolder("INBOX");
                                    inbox.open(Folder.READ_WRITE);

                                    SearchTerm senderTerm = new FromTerm(new InternetAddress(hablaxServerEmail));
                                    SearchTerm seenTerm = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
                                    Message[] messages = inbox.search(new AndTerm(senderTerm, seenTerm));

                                    long lastKnownMessageUID = 0;
                                    JSONArray mails = new JSONArray();
                                    for (Message message : messages) {
                                        JSONObject mail = javascriptJavaMessage(inbox, message);
                                        mails.put(mail);
                                        lastKnownMessageUID = message.getMessageNumber();

                                        if (mail.getBoolean("hasAttachment")) {
                                            continue;
                                        }

                                        Flags flags = new Flags(Flags.Flag.DELETED);
                                        flags.add(Flags.Flag.SEEN);
                                        message.setFlags(flags,true);
                                    }


                                    store.close();

                                    JSONObject result = new JSONObject();
                                    result.put("new_messages", mails);
                                    result.put("last_known_message_uid", lastKnownMessageUID);
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.OK, result);
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                } catch (NoSuchProviderException e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                } catch (MessagingException e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                } catch (JSONException e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                } catch (IOException e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                }
                                return true;
                            }

                        };
                        syncMessagesTask.execute();
                        PluginResult dataResult = new PluginResult(PluginResult.Status.NO_RESULT);
                        dataResult.setKeepCallback(true);
                        command.sendPluginResult(dataResult);




                    } catch (JSONException e) {
                        PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                        dataResult.setKeepCallback(false);
                        command.sendPluginResult(dataResult);
                    }
                } else if (action.equals("fetchAttachment")) {
                    try {
                        final String hostname = args.get(0).toString();
                        final String port = args.get(1).toString();
                        final String username = args.get(2).toString();
                        final String password = args.get(3).toString();
                        final String connectionType = args.get(4).toString();
                        final long messageUID = Long.parseLong(args.get(5).toString());
                        final String partID = args.get(6).toString();
                        final int partEncoding = Integer.parseInt(args.get(7).toString());
                        final String filename = args.get(8).toString();

                        AsyncTask<String, Integer, Boolean> fetchAttachmentTask = new AsyncTask<String, Integer, Boolean>() {
                            @Override
                            protected Boolean doInBackground(String[] objects) {
                                try {
                                    IMAPStore store = null;

                                    Properties properties = new Properties();
                                    if (hostname.equals("imap.nauta.cu")) {

                                        properties.put("mail.imap.ssl.enable", false);
                                        properties.put("mail.imap.timeout", "60000");
                                        Session session = Session.getDefaultInstance(properties, null);
                                        store = (IMAPStore)session.getStore("imap");

                                    } else if (hostname.equals("siamopizza.com")) {

//                                        properties.put("mail.store.protocol", "imaps");
//                                        properties.put("mail.imap.host", hostname);
//                                        properties.put("mail.imap.auth", "true");
//                                        properties.put("mail.imap.port", port);

                                        properties.put("mail.imap.ssl.enable", false);
                                        properties.put("mail.imap.timeout", "60000");
                                        properties.put("mail.debug", "true");

                                        Session session = Session.getDefaultInstance(properties, null);
                                        store = (IMAPStore)session.getStore("imap");
                                    } else {

                                        properties.setProperty("mail.imap.host", hostname);
                                        properties.setProperty("mail.imap.port", port);

                                        properties.put("mail.imap.ssl.enable", true);
                                        properties.setProperty("mail.imap.timeout", "60000");

                                        properties.put("mail.debug", "true");

                                        Session session = Session.getDefaultInstance(properties, null);
                                        store = (IMAPStore)session.getStore("imaps");
                                    }

                                    store.connect(hostname, username, password);
                                    IMAPFolder inbox = (IMAPFolder)store.getFolder("INBOX");
                                    inbox.open(Folder.READ_WRITE);

                                    String attachmentPath = null;

                                    Message message = inbox.getMessageByUID(messageUID);
                                    if (message != null) {
										String contentType = message.getContentType();
										if (contentType.equalsIgnoreCase("text/plain")) {
                                            // nothing
										} else if (contentType.contains("multipart")) {
											InputStream content = null;
											Multipart multiPart = (Multipart) message.getContent();
											int n = multiPart.getCount();
											for (int partCount = 0; partCount < n; partCount++) {
												MimeBodyPart bodyPart = (MimeBodyPart) multiPart.getBodyPart(partCount);
												
												if (!Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition())) {
													continue;
												}
												
												content = bodyPart.getInputStream();
												if(content != null) {
													String dir = Environment.getExternalStorageDirectory().toString();
													File file = new File(dir, filename);
													OutputStream fos = new FileOutputStream(file);
													int len = 0;
													byte[] buf = new byte[4096];
													while((len=content.read(buf)) != -1) {
														fos.write(buf, 0, len);
													}
													fos.close();
													attachmentPath = file.getAbsolutePath();
												}
												break;
											}
                                        } else { // multipart
											InputStream content = (InputStream) message.getContent();
											String dir = Environment.getExternalStorageDirectory().toString();
											File file = new File(dir, filename);
											OutputStream fos = new FileOutputStream(file);
											int len = 0;
											byte[] buf = new byte[4096];
											while((len=content.read(buf)) != -1) {
												fos.write(buf, 0, len);
											}
											fos.close();
											attachmentPath = file.getAbsolutePath();
                                        }

                                        Flags flags = new Flags(Flags.Flag.DELETED);
                                        flags.add(Flags.Flag.SEEN);
                                        message.setFlags(flags,true);
                                    }

                                    store.close();

                                    JSONObject result = new JSONObject();
                                    result.put("attachmentPath", attachmentPath);
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.OK, result);
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                } catch (NoSuchProviderException e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                } catch (MessagingException e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                } catch (JSONException e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                } catch (IOException e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                } catch (Exception e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                }
                                return true;
                            }

                        };
                        fetchAttachmentTask.execute();
                        PluginResult dataResult = new PluginResult(PluginResult.Status.NO_RESULT);
                        dataResult.setKeepCallback(true);
                        command.sendPluginResult(dataResult);

                    } catch (JSONException e) {
                        PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                        dataResult.setKeepCallback(false);
                        command.sendPluginResult(dataResult);
                    }


                } else if (action.equals("imapFetchAllMessages")) {

                    try {
                        final String hostname = args.get(0).toString();
                        final String port = args.get(1).toString();
                        final String username = args.get(2).toString();
//                        final String username = "vinh.thien0301@gmail.com";
                        final String password = args.get(3).toString();
//                        final String password = "taonekon8776941";
                        final String connectionType = args.get(4).toString();

                        AsyncTask<String, Integer, Void> fetchMessagesTask = new AsyncTask<String, Integer, Void>() {
                            @Override
                            protected Void doInBackground(String... strings) {
                                try {
                                    String storeType = "imaps";

                                    Properties properties = new Properties();

                                    IMAPStore store = null;

                                    if (hostname.equals("imap.nauta.cu")) {
                                        if (connectionType.equals("S1")) {
                                            // 1
                                            properties.put("mail.imap.ssl.enable", false);
                                            properties.put("mail.imap.timeout", "60000");
                                            Session session = Session.getDefaultInstance(properties, null);
                                            store = (IMAPStore)session.getStore("imap");
                                        }

                                        if (connectionType.equals("S2")) {
                                            // 2
                                            properties.put("mail.imap.ssl.enable", false);
                                            properties.put("mail.imap.timeout", "60000");
                                            Session session = Session.getDefaultInstance(properties, null);
                                            store = (IMAPStore)session.getStore("imaps");
                                        }

                                        if (connectionType.equals("S3")) {
                                            // 3
                                            properties.put("mail.imap.starttls.enable", "false");
                                            properties.put("mail.imap.timeout", "60000");
                                            Session session = Session.getDefaultInstance(properties, null);
                                            store = (IMAPStore)session.getStore("imap");
                                        }

                                        if (connectionType.equals("S4")) {
                                            // 4
                                            properties.put("mail.imap.starttls.enable", "false");
                                            properties.put("mail.imap.ssl.enable", false);
                                            properties.put("mail.imap.timeout", "60000");
                                            Session session = Session.getDefaultInstance(properties, null);
                                            store = (IMAPStore)session.getStore("imap");
                                        }

                                        if (connectionType.equals("Clear")) {
                                            properties.put("mail.imap.ssl.enable", false);
                                            properties.put("mail.imap.timeout", "60000");
                                            Session session = Session.getDefaultInstance(properties, null);
                                            store = (IMAPStore)session.getStore("imap");
                                        }

                                    } else {


//                                        properties.setProperty("mail.store.protocol", "imaps");
                                        properties.setProperty("mail.imap.host", hostname);
                                        properties.setProperty("mail.imap.port", port);

                                        properties.put("mail.imap.ssl.enable", true);
                                        properties.setProperty("mail.imap.timeout", "60000");

                                        properties.put("mail.debug", "true");

                                        Session session = Session.getDefaultInstance(properties, null);
                                        store = (IMAPStore)session.getStore("imaps");
                                    }




                                    store.connect(hostname, username, password);

                                    IMAPFolder emailFolder = (IMAPFolder)store.getFolder("INBOX");
                                    emailFolder.open(Folder.READ_ONLY);

                                    FlagTerm unseenFlagTerm = new FlagTerm(new Flags(
                                            Flags.Flag.SEEN), false);
                                    FromTerm fromTerm = new FromTerm(new InternetAddress("hablax.demo@gmail.com"));
                                    SearchTerm searchTerm = new AndTerm(unseenFlagTerm, fromTerm);

                                    Message[] messages = emailFolder.search(searchTerm);

//                                    JSONArray mails = new JSONArray();
//                                    for (Message message : messages) {
//                                        JSONObject mail = javascriptJavaMessage(message);
//                                        mails.put(mail);
//                                    }
                                    JSONObject result = new JSONObject();
//                                    result.put("mails", mails);
                                    result.put("number_mails", messages.length);

                                    PluginResult dataResult = new PluginResult(PluginResult.Status.OK, result);
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);

                                    emailFolder.close(false);
                                    store.close();





                                } catch (NoSuchProviderException e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                } catch (MessagingException e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                } catch (Exception e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                }
                                return null;

                            }
                        };
                        fetchMessagesTask.execute();

                        PluginResult dataResult = new PluginResult(PluginResult.Status.NO_RESULT);
                        dataResult.setKeepCallback(true);
                        command.sendPluginResult(dataResult);

                    } catch (JSONException e) {
                        PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                        dataResult.setKeepCallback(false);
                        command.sendPluginResult(dataResult);
                    }



                }
                else if (action.equals("smtpSendMessage")) {


                    try {
                        final String hostname = args.get(0).toString();
                        final Integer portNumber = (Integer)args.get(1);
                        String port = portNumber.toString();
                        final String username = args.get(2).toString();
                        final String password = args.get(3).toString();
                        String connectionType = args.get(4).toString();
                        final String recipient = args.get(5).toString();
                        final String subject = args.get(6).toString();
                        final String body = args.get(7).toString();
                        final String attachmentPath = args.get(8).toString();


                        Properties props = new Properties();
                        if (hostname.equals("smtp.nauta.cu")) {
                            props.setProperty("mail.transport.protocol", "smtp");
                            props.setProperty("mail.host", hostname);

                            props.put("mail.smtp.host", hostname);
                            props.put("mail.smtp.auth", "true");
                            props.put("mail.smtp.port", port);
                            props.put("mail.smtp.timeout", "60000");
                            props.put("mail.smtp.ssl.enable", false);

                        } else if (hostname.equals("siamopizza.com")) {

                            props.setProperty("mail.transport.protocol", "smtp");
                            props.setProperty("mail.host", hostname);

                            props.put("mail.smtp.host", hostname);
                            props.put("mail.smtp.auth", "true");
                            props.put("mail.smtp.port", port);
                            props.put("mail.smtp.timeout", "60000");
                            props.put("mail.smtp.ssl.enable", false);
                            props.put("mail.debug", "true");

                        } else {



                            props.setProperty("mail.transport.protocol", "smtp");
                            props.setProperty("mail.host", hostname);
                            props.put("mail.smtp.host", hostname);
                            props.put("mail.smtp.auth", "true");
                            props.put("mail.smtp.port", port);
                            props.put("mail.smtp.starttls.enable", "true");

                            props.put("mail.smtp.socketFactory.port", port);
                            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
                            props.put("mail.smtp.socketFactory.fallback", "false");

                            props.put("mail.debug", "true");

                        }

                        Authenticator authenticator = new Authenticator() {
                            @Override
                            protected PasswordAuthentication getPasswordAuthentication() {
                                return new PasswordAuthentication(username, password);
                            }
                        };

                        final Session session = Session.getInstance(props, authenticator);


                        AsyncTask<String, Integer, Void> sendMailTask = new AsyncTask<String, Integer, Void>() {
                            @Override
                            protected Void doInBackground(String[] objects) {
                                try {
                                    MailCorePlugin.sendMail(session, subject, body, hostname, portNumber, username, password, recipient, attachmentPath);

                                    JSONObject result = new JSONObject();
                                    result.put("message", "Sent successfully");
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.OK, result);
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                } catch (AddressException e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                } catch (MessagingException e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                } catch (JSONException e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                } catch (IOException e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                }
                                return null;
                            }
                        };
                        sendMailTask.execute();

                        PluginResult dataResult = new PluginResult(PluginResult.Status.NO_RESULT);
                        dataResult.setKeepCallback(true);


                    } catch (JSONException e) {
                        command.error(e.getMessage());
                    }

                }
                else if (action.equals("listenNewMessages")) {
                    try {
                        final String hostname = args.get(0).toString();
                        final String port = args.get(1).toString();
                        final String username = args.get(2).toString();
                        final String password = args.get(3).toString();
                        final String connectionType = args.get(4).toString();
                        final long lastMessageKnownUID = Long.parseLong(args.get(5).toString());
                        final String hablaxServerEmail = args.get(6).toString();
                        final JSONArray hablaxEmailsAccepted;
                        if (args.get(7) instanceof JSONArray) {
                            hablaxEmailsAccepted = (JSONArray)args.get(7);
                        } else if (args.get(7) instanceof JSONObject) {
                            hablaxEmailsAccepted = null;
                        }


                        AsyncTask<String, Integer, Boolean> listenNewMessagesTask = new AsyncTask<String, Integer, Boolean>() {
                            @Override
                            protected Boolean doInBackground(String[] objects) {
                                try {

                                    IMAPStore store = null;

                                    Properties properties = new Properties();
                                    if (hostname.equals("imap.nauta.cu")) {

                                        properties.put("mail.imap.ssl.enable", false);
                                        properties.put("mail.imap.timeout", "60000");
                                        Session session = Session.getDefaultInstance(properties, null);
                                        store = (IMAPStore)session.getStore("imap");

                                    } else if (hostname.equals("siamopizza.com")) {
//                                        properties.put("mail.store.protocol", "imaps");
//                                        properties.put("mail.imap.host", hostname);
//                                        properties.put("mail.imap.auth", "true");
//                                        properties.put("mail.imap.port", port);
                                        properties.put("mail.imap.ssl.enable", false);
                                        properties.put("mail.imap.timeout", "60000");
                                        properties.put("mail.debug", "true");

                                        Session session = Session.getDefaultInstance(properties, null);
                                        store = (IMAPStore)session.getStore("imap");
                                    } else {

//                                        properties.setProperty("mail.store.protocol", "imaps");
                                        properties.setProperty("mail.imap.host", hostname);
                                        properties.setProperty("mail.imap.port", port);

                                        properties.put("mail.imap.ssl.enable", true);
                                        properties.setProperty("mail.imap.timeout", "60000");

                                        properties.put("mail.debug", "true");

                                        Session session = Session.getDefaultInstance(properties, null);
                                        store = (IMAPStore)session.getStore("imaps");
                                    }

                                    store.connect(hostname, username, password);
                                    final IMAPFolder folder = (IMAPFolder)store.getFolder("Inbox");

                                    final MessageCountListener listener = new MessageCountListener() {

                                        @Override
                                        public void messagesAdded(MessageCountEvent messageCountEvent) {
                                            try {

                                                JSONObject result = new JSONObject();
                                                result.put("new_messages_notified", true);


                                                long lastKnownMessageUID = 0;
                                                JSONArray mails = new JSONArray();
                                                for (Message message : messageCountEvent.getMessages()) {
                                                    if (message.isSet(Flags.Flag.SEEN)) {
                                                        continue;
                                                    }
                                                    if (message.isSet(Flags.Flag.DELETED)) {
                                                        continue;
                                                    }

                                                    JSONObject mail = javascriptJavaMessage(folder, message);

                                                    if (!mail.getString("from").equalsIgnoreCase(hablaxServerEmail)) {
                                                        continue;
                                                    }

                                                    mails.put(mail);
                                                    lastKnownMessageUID = message.getMessageNumber();

                                                    if (mail.getBoolean("hasAttachment")) {
                                                        continue;
                                                    }

                                                    Flags flags = new Flags(Flags.Flag.DELETED);
                                                    flags.add(Flags.Flag.SEEN);
                                                    message.setFlags(flags, true);
                                                }

                                                result = new JSONObject();
                                                result.put("new_messages", mails);
                                                result.put("last_known_message_uid", lastKnownMessageUID);
                                                PluginResult dataResult = new PluginResult(PluginResult.Status.OK, result);
                                                dataResult.setKeepCallback(true);
                                                command.sendPluginResult(dataResult);
                                            } catch (MessagingException e) {
                                                PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                                dataResult.setKeepCallback(true);
                                                command.sendPluginResult(dataResult);
                                            } catch (JSONException e) {
                                                PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                                dataResult.setKeepCallback(true);
                                                command.sendPluginResult(dataResult);
                                            } catch (IOException e) {
                                                PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                                dataResult.setKeepCallback(true);
                                                command.sendPluginResult(dataResult);
                                            }
                                        }

                                        @Override
                                        public void messagesRemoved(MessageCountEvent messageCountEvent) {
                                            // nothing
                                        }
                                    };

                                    folder.addMessageCountListener(listener);
                                    folder.open(Folder.READ_WRITE);

                                    if (idleThread != null) {
                                        idleThread.kill();
                                    }

                                    idleThread = new IdleThread(folder,username,password);
                                    idleThread.setDaemon(false);
                                    idleThread.setListener(new IdleThread.IdleListener() {
                                        @Override
                                        public void afterIdle() {
                                            PluginResult dataResult = new PluginResult(PluginResult.Status.OK, "listen");
                                            dataResult.setKeepCallback(true);
                                            command.sendPluginResult(dataResult);
                                        }

                                        @Override
                                        public void beforeIdle() {
                                            PluginResult dataResult = new PluginResult(PluginResult.Status.OK, "before");
                                            dataResult.setKeepCallback(true);
                                            command.sendPluginResult(dataResult);
                                        }

                                    });

                                    PluginResult dataResult = new PluginResult(PluginResult.Status.OK, "before");
                                    dataResult.setKeepCallback(true);
                                    command.sendPluginResult(dataResult);

                                    idleThread.start();

                                }
                                catch (MessagingException e) {
                                    PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                    dataResult.setKeepCallback(false);
                                    command.sendPluginResult(dataResult);
                                }
                                return true;
                            }
                        };
                        listenNewMessagesTask.execute();
                        PluginResult dataResult = new PluginResult(PluginResult.Status.NO_RESULT);
                        dataResult.setKeepCallback(true);
                        command.sendPluginResult(dataResult);


                    } catch (JSONException e) {
                        PluginResult dataResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                        dataResult.setKeepCallback(false);
                        command.sendPluginResult(dataResult);
                    }
                }
                else if (action.equals("killListenerOperations")) {

//                    if (idleOperations != null && !idleOperations.isEmpty()) {
//                        for (IMAPIdleOperation idleOperation : idleOperations) {
//                            if (!idleOperation.isCancelled()) {
//                                idleOperation.interruptIdle();
//                            }
//
//                        }
//                        idleOperations.clear();
//                    }
                }
            }
        });

        return true;
    }

    protected JSONObject javascriptJavaMessage(IMAPFolder folder, Message message) throws JSONException, MessagingException, IOException {
        JSONObject response = new JSONObject();
        if (message == null) {
            return response;
        }

        response.put("uid", folder.getUID(message));
        if (message.getFrom() != null && message.getFrom().length > 0) {
            javax.mail.Address address = message.getFrom()[0];
            response.put("from", address.toString());
        } else {
            response.put("from", "");
        }

        if (message.getAllRecipients() != null && message.getAllRecipients().length > 0) {
            javax.mail.Address address = message.getAllRecipients()[0];
            response.put("to", address.toString());
        } else {
            response.put("to", "");
        }

        response.put("subject", message.getSubject());
        response.put("body", message.getSubject());
        response.put("date", message.getReceivedDate());


        boolean hasAttachment = false;

        String partID = "";
        int partEncoding = 0;
        String filename = "";
        if (message.getContent() != null) {
            if (message.getContentType().equalsIgnoreCase("text/plain")) {
                String text = message.getContent().toString();
            } else { // multipart/*
                filename = message.getFileName();
                hasAttachment = true;
            }
        }
        response.put("hasAttachment", hasAttachment);
        response.put("partID", partID);
        response.put("partEncoding", partEncoding);
        response.put("filename", filename);

        return response;
    }


    protected static class IdleThread extends Thread {
        private final Folder folder;
        private volatile boolean running = true;
        private final String username;
        private final String password;

        public interface IdleListener {
            void afterIdle();
            void beforeIdle();
        }

        private IdleListener listener;

        public IdleThread(Folder folder,String username,String password) {
            super();
            this.folder = folder;
            this.username = username;
            this.password = password;
        }

        public void setListener(IdleListener listener) {
            this.listener = listener;
        }

        public synchronized void kill() {

            if (!running)
                return;
            this.running = false;
        }

        @Override
        public void run() {
            while (running) {

                try {
                    ensureOpen(folder,username,password);
                    System.out.println("enter idle");
                    listener.beforeIdle();
                    ((IMAPFolder) folder).idle();
                    listener.afterIdle();
                } catch (Exception e) {
                    // something went wrong
                    // wait and try again
                    e.printStackTrace();
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e1) {
                        // ignore
                    }
                }

            }
        }
    }

    public static void sendMail(Session smtpSession, String subject, String body, String host,
                                int port, String sender, String password, String recipients, String attachmentPath)
            throws IOException, MessagingException {
        // The message object
        MimeMessage message = new MimeMessage(smtpSession);

        // Add trivial information
//        message.setSender(new InternetAddress(sender));
        message.setFrom(new InternetAddress(sender));
        message.setSentDate(new Date());
        message.setSubject(subject);
        message.setHeader("X-HL-Version-Android", "v1");
        if (recipients.indexOf(',') > 0) {
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipients));
        } else {
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipients));
        }

        // Create multipart content.
        Multipart multipart = new MimeMultipart("mixed");

        // Part two is attachment
        if (attachmentPath != null && attachmentPath != "null") {
            attachmentPath = attachmentPath.substring(7);

            MimeBodyPart attachmentBodyPart = new MimeBodyPart();

            attachmentBodyPart.attachFile(attachmentPath);
            attachmentBodyPart.setContentID("<img>");

//            attachmentBodyPart.setDisposition(MimeBodyPart.INLINE);
//            attachmentBodyPart.setDataHandler(new DataHandler(source));
//            messageBodyPart.setFileName("file.png");

            multipart.addBodyPart(attachmentBodyPart);
        } else {
            // Add the body part
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(body.getBytes(), "text/plain")));
            multipart.addBodyPart(messageBodyPart);
        }

        message.setContent(multipart);

        // Send the message.
//        Transport.send(message);


        Transport transport = smtpSession.getTransport("smtp");
        transport.connect(host, port, sender, password);
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
    }

    public static final class JSSEProvider extends Provider {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        public JSSEProvider() {
            super("HarmonyJSSE", 1.0, "Harmony JSSE Provider");
            AccessController.doPrivileged(new java.security.PrivilegedAction<Void>() {
                public Void run() {
                    put("SSLContext.TLS",
                            "org.apache.harmony.xnet.provider.jsse.SSLContextImpl");
                    put("Alg.Alias.SSLContext.TLSv1", "TLS");
                    put("KeyManagerFactory.X509",
                            "org.apache.harmony.xnet.provider.jsse.KeyManagerFactoryImpl");
                    put("TrustManagerFactory.X509",
                            "org.apache.harmony.xnet.provider.jsse.TrustManagerFactoryImpl");
                    return null;
                }
            });
        }
    }

    public static void close(final Folder folder) {
        try {
            if (folder != null && folder.isOpen()) {
                folder.close(false);
            }
        } catch (final Exception e) {
            // ignore
        }

    }

    public static void close(final Store store) {
        try {
            if (store != null && store.isConnected()) {
                store.close();
            }
        } catch (final Exception e) {
            // ignore
        }

    }

    public static void ensureOpen(final Folder folder,String username,String password) throws MessagingException {

        if (folder != null) {
            Store store = folder.getStore();
            if (store != null && !store.isConnected()) {
                store.connect(username, password);
            }
        } else {
            throw new MessagingException("Unable to open a null folder");
        }

        if (folder.exists() && !folder.isOpen() && (folder.getType() & Folder.HOLDS_MESSAGES) != 0) {
            System.out.println("open folder " + folder.getFullName());
            folder.open(Folder.READ_ONLY);
            if (!folder.isOpen())
                throw new MessagingException("Unable to open folder " + folder.getFullName());
        }

    }

}

