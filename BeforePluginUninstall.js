#!/usr/bin/env node
'use strict';

let cwd = process.cwd();
let fs = require('fs');
let path = require('path');

console.log('InstagramAssetsPicker BeforePluginInstall.js, attempting to modify build.xcconfig');

let xcConfigBuildFilePath = path.join(cwd, 'platforms', 'ios', 'cordova', 'build.xcconfig');
console.log('xcConfigBuildFilePath: ', xcConfigBuildFilePath);
let lines = fs.readFileSync(xcConfigBuildFilePath, 'utf8').split('\n');

let headerSearchPathLineNumber;
lines.forEach((l, i) => {
  if (l.indexOf('HEADER_SEARCH_PATHS') > -1) {
  headerSearchPathLineNumber = i;
}
});

let line = lines[headerSearchPathLineNumber];
console.log("HEADER_SEARCH_PATHS before:"+line);
lines[headerSearchPathLineNumber] = line.replace(/\ "\$\(SRCROOT\)\/..\/..\/plugins\/com.hablax.mailcore\/src\/ios\/include"/i, '');
console.log("HEADER_SEARCH_PATHS after:"+line);
let newConfig = lines.join('\n');

fs.writeFile(xcConfigBuildFilePath, newConfig, function (err) {
  if (err) {
    console.log('error updating build.xcconfig, err: ', err);
    return;
  }
  console.log('successfully updated HEADER_SEARCH_PATHS in build.xcconfig');
});




let otherLDFlagsLineNumber;
lines.forEach((l, i) => {
  if (l.indexOf('OTHER_LDFLAGS') > -1) {
  otherLDFlagsLineNumber = i;
}
});

line = lines[otherLDFlagsLineNumber];
console.log("OTHER_LDFLAGS before:"+line);

line = line.replace(/ -lxml2 -liconv -lz -lc\+\+ -lresolv -stdlib=libc\+\+ -ObjC/i, '');
lines[otherLDFlagsLineNumber] = line;
console.log("OTHER_LDFLAGS after:"+line);
newConfig = lines.join('\n');

fs.writeFile(xcConfigBuildFilePath, newConfig, function (err) {
  if (err) {
    console.log('error updating build.xcconfig, err: ', err);
    return;
  }
  console.log('successfully updated OTHER_LDFLAGS in build.xcconfig');
});

